# READ ME #
---

This Repository is used for checking the integrity of Pace Customized Forms after an upgrade of Pace.

## Important Links ##

Pace Staging [User Defined Forms](http://205.207.123.114/epace/company:public/object/UserDefinedForm/list) 

Pace Staging [User Defined Includable Forms](http://205.207.123.114/epace/company:public/object/UserDefinedIncludableForm/list) 

Google Sheet - [Custom Forms Catalogue](https://docs.google.com/spreadsheets/d/1m3bNPQpVffQbE7kH0hRsX_Sj4ZxPAwyydcSAkwJUn58/edit#gid=963547434)


Pace Live [User Defined Forms](http://205.207.123.118/epace/company:public/object/UserDefinedForm/list) 

Pace Live [User Defined Includable Forms](http://205.207.123.118/epace/company:public/object/UserDefinedIncludableForm/list)

## How do I get set up? ##

* Install Git and use terminal commands to fetch files and commit changes
* or install [Source Tree](https://www.sourcetreeapp.com/) client to manage Git commands
* Require a text editor such as Text Wrangler or Aptana Studio

## Notes ##

default-files branch: 
contains the most current code for default Pace User Forms XML

customized-files branch:
contains the most current code for customized Pace User Forms XML

* Always update Pace Staging version first and check your customizations' integrity there
* In simple terms, you will be checking if there are any differences between the code of your saved default-files and the code of the default-files in the new version of Pace that you have just installed
* If there are differences between defaults, then your customizations are fine to carry forward
* If there are differences, then you will have to update your customizations based on the new defaults

## Instructions ##
---

#### Comparing default-files ####

1. `CHECKOUT` default-files in Git
2. In the Pace user defined forms list, delete the customization that you wish to check
3. Add this customization back in to reset its code to default
4. Drill In and Copy this code
5. Paste code overtop of the corresponding default file's code in your text editor and click `SAVE`
6. If Git does not detect any differences, then the default of the page's code has not changed and your customization is fine to carry forward
7. Copy the code from Bitbucket for the customized version of that file and paste into the Pace Customized Form
8. If Git detected changes, you will have to `COMMIT` in Git in order to update your default-files branch.
   You will now have to update your customizations to incorporate whatever is new in the Pace code  

#### Updating customized-files ####

1. `CHECKOUT` customized-files in Git
2. Copy your new default code from bitbucket
3. Paste code overtop of the corresponding customized files' code in your text editor and click `SAVE`
4. This will result in multiple diffs
5. Highlight and `STAGE` the lines that you want to keep from the new default
6. `DISCARD` the line remaining lines of code as they are your customizations that you want to carry forward
7. This process will result in merging your old customized file with any new code that has been introduced in the Pace upgrade

